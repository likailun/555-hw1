package edu.upenn.cis.cis455.m1.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;

import org.apache.logging.log4j.Level;

public class TestGetFile {
//    @Before
//    public void setUp() {
//        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
//    }
    
    String sampleGetRequest1 = 
            "GET /index.html" +
            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
            "Host: www.cis.upenn.edu\r\n" +
            "Accept-Language: en-us\r\n" +
            "Accept-Encoding: gzip, deflate\r\n" +
            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
            "Connection: Keep-Alive\r\n\r\n";
    @Test
    public void test2() throws Exception {
        HttpWorker worker = new HttpWorker(null, 2);
        worker.stopEachWorker();
        assertTrue(worker.stopping == true);
    }

    @Test
    public void test3() throws Exception {
        HttpTaskQueue queue = new HttpTaskQueue(5);
        System.out.println(queue.size());
        assertTrue(queue.size() == 0);
    }
    
    @Test
    public void test4() throws Exception {
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      Socket s = TestHelper.getMockSocket(
          sampleGetRequest1, 
          byteArrayOutputStream);

      HttpTask task = new HttpTask(s,"./www",null);
      task.run();
      String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
      System.out.println(result);
      
      assertTrue(result.contains("404"));
    }
    
    @Test
    public void test5() throws Exception {
        HttpListener listener = new HttpListener(10,10);
        listener.run();
        assertTrue(listener.threadsPool.get(1).status.equals("waiting"));
    }

    @Test
    public void test6() throws Exception {
        WebService webService = new WebService(2340, "./www");
        webService.serverSocket = new ServerSocket(43332);
        webService.stop();
        assertTrue(webService.serverSocket.isClosed());
    }
    
    @After
    public void tearDown() {}
}
