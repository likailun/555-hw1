/**
 * CIS 455/555 route-based HTTP framework
 * 
 * Z. Ives, 8/2017
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m2.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.interfaces.Session;
import edu.upenn.cis.cis455.m2.interfaces.SessionHandler;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpListener;
import edu.upenn.cis.cis455.m2.interfaces.HttpTask;
import edu.upenn.cis.cis455.m2.interfaces.Filter;

public class WebService  {
    final static Logger logger = LogManager.getLogger(WebService.class);
	public static int portNum;
	public static String rootD;
	// change number of threads and max size of shared queue
	public static int numThreads = 1;
	public static int sizeSharedQ = 10000;
	public static ServerSocket serverSocket;
	public static HashMap<String, Route> routeMap = new HashMap<>();
	public static HashMap<String, Filter> beforeMap = new HashMap<>();
	public static HashMap<String, Filter> afterMap = new HashMap<>();
	public static HashMap<String, Session> sessionMap = new HashMap<>();

	public WebService(int port, String rootDirect) {
		System.out.println("inside webservice port and rootDirect are:" + rootDirect);
		this.portNum = port; 
		this.rootD = rootDirect;
	}
	
    protected HttpListener listener;

    /**
     * Launches the Web server thread pool and the listener
     * @throws Exception 
     */
    public void start() throws Exception {
        serverSocket = new ServerSocket(portNum);
        listener = new HttpListener(numThreads, sizeSharedQ);
        listener.run();
		while (true) {
			Socket clientSocket = serverSocket.accept();
			System.out.println("WebService.java starts");
			System.out.println(" Webservice.java Accepted connection: " + clientSocket);
			listener.addNewTask(new HttpTask(clientSocket, rootD, listener.threadsPool));
		}
    }

    /**
     * Gracefully shut down the server
     * @throws Exception 
     */
    public static void stop() throws Exception {
    	System.out.println("WebService: stop the server");
    	serverSocket.close();
    	System.out.println("WebService: all system closed");
    	return;
    }

    /**
     * Hold until the server is fully initialized.
     * Should be called after everything else.
     * @throws Exception 
     */
    public void awaitInitialization() throws Exception {
        logger.info("Initializing server");
        start();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt() {
        throw new HaltException();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode) {
        throw new HaltException(statusCode);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(String body) {
        throw new HaltException(body);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode, String body) {
        throw new HaltException(statusCode, body);
    }

    ////////////////////////////////////////////
    // Server configuration
    ////////////////////////////////////////////

    /**
     * Set the root directory of the "static web" files
     */
    public void staticFileLocation(String directory) {
    }

    /**
     * Set the IP address to listen on (default 0.0.0.0)
     */
    public void ipAddress(String ipAddress) {}

    /**
     * Set the TCP port to listen on (default 45555)
     */
    public void port(int port) {}

    /**
     * Set the size of the thread pool
     */
    public void threadPool(int threads) {}

    ///////////////////////////////////////////////////
    // For more advanced capabilities
    ///////////////////////////////////////////////////

    /**
     * Handle an HTTP POST request to the path
     */
    
    public void post(String path, Route route) {
    	routeMap.put("POST"+path, route);
    }
    
    public void get(String path, Route route) {
    	routeMap.put("GET"+path, route);
    }

    /**
     * Handle an HTTP PUT request to the path
     */
    public void put(String path, Route route) {
    	routeMap.put("PUT"+path, route);
    }

    /**
     * Handle an HTTP DELETE request to the path
     */
    public void delete(String path, Route route) {
    	routeMap.put("DELETE"+path, route);
    }

    /**
     * Handle an HTTP HEAD request to the path
     */
    public void head(String path, Route route) {
    	routeMap.put("HEAD"+path, route);
    }

    /**
     * Handle an HTTP OPTIONS request to the path
     */
    public void options(String path, Route route) {
    	routeMap.put("OPTIONS"+path, route);
    }

    ///////////////////////////////////////////////////
    // HTTP request filtering
    ///////////////////////////////////////////////////

    /**
     * Add filters that get called before a request
     */
    public void before(Filter filter) {
    	beforeMap.put("", filter);
    }

    /**
     * Add filters that get called after a request
     */
    public void after(Filter filter) {
    	afterMap.put("", filter);
    }

    /**
     * Add filters that get called before a request
     */
    public void before(String path, Filter filter) {
    	beforeMap.put(path, filter);
    }

    /**
     * Add filters that get called after a request
     */
    public void after(String path, Filter filter) {
    	afterMap.put(path, filter);
    }
    
    public static Session getSession(String id) {
    	return sessionMap.get(id);
    }
    
    public static String createSession() {
		long createTime = System.currentTimeMillis() / 1000;
		Session session = new SessionHandler(createTime);
		String id = String.valueOf(session.hashCode());
		session.setId(id);
    	sessionMap.put(id, session);
    	return id;
    }
    
    public static void deleteSession(String id) {
    	sessionMap.remove(id);
    }

}
