package edu.upenn.cis.cis455.m2.interfaces;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.cis455.m2.server.WebService;

public class RequestHandler1 extends Request {

	public static String version = "HTTP/1.0";
	public static String filePath;
	public static String method;
	public static HashMap<String, String> headersMap = new HashMap<>();
	public static BufferedReader inputRequest;
	public static Boolean badrequest = false;
	public static Boolean notImplemented = false;
	public static String body = null;
	public static Map<String, Object> attributeMap = new LinkedHashMap<String, Object>();
	public static Map<String, String> paramMap = new LinkedHashMap<String, String>();
	public static Map<String, String> queryParamMap = new LinkedHashMap<String, String>();
	public static Map<String, String> cookieMap = new LinkedHashMap<String, String>();
	public static Session currentSession;
	public static String sessionId = null;
	
	public RequestHandler1(BufferedReader inputRequest) {
		this.inputRequest = inputRequest;
	}
	
	
	public void startParse() throws Exception {
		System.out.println("RequestHandler: start parsing");
		getFirst();
		getHeader();
		checkSessionRouting();
		//		getBody();
		System.out.println("RequestHandler: file path is: " + filePath 
				+ "method is:" + method +"\n"
				+ "header is " + Arrays.asList(headersMap)
				);	
		checkFirst();
	}
	
	public void getFirst() throws Exception {
		String firstInput = this.inputRequest.readLine();
		String[] firstTemp;
		if (firstInput != null) {
			firstTemp = firstInput.split("\\s+");
			
			if (firstTemp.length < 3) {
				badrequest = true;
			}
			else {
			this.method = firstTemp[0];
			this.filePath = firstTemp[1];
			this.version = firstTemp[2];
			}
		}
		if(firstInput == null) {
			badrequest = true;
		}
		
	}
	
	public void getBody() throws Exception {
	    StringBuilder stringBuilder = new StringBuilder();
	    char[] charBuffer = new char[128];
	    int bytesRead = -1;
	    if (this.inputRequest.ready() == false) {
	    	return;
	    }
	    while ((bytesRead = inputRequest.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
        
	    this.body = stringBuilder.toString();
	    System.out.println("RequestHandler: body is " + this.body);
	}
	
	public void getHeader() throws Exception{
		String line = inputRequest.readLine();
		String lastKey = null;
        while (line != null && !line.trim().isEmpty()) {
            int p = line.indexOf(':');
            if (p >= 0) {
            	String key = line.substring(0, p).trim().toLowerCase(Locale.US);
            	String value = line.substring(p + 1).trim();
            	lastKey = key;
            	headersMap.put(key, value);
            } else if (lastKey != null && line.startsWith(" ") || line.startsWith("\t")) {
                String newPart = line.trim();
                headersMap.put(lastKey, headersMap.get(lastKey) + newPart);
            }
            line = inputRequest.readLine();
        }
	}
	
	public void checkFirst() {
		checkHost();
		checkMethod();
	}
	
	public void checkHost() {
		if(version.equals("HTTP/1.1") && host()==null) {
			badrequest = true;
		}
	}
	
	// check if get or head
	public void checkMethod() {
		if (method.equals("GET") | method.equals("HEAD")| method.equals("DELETE")| method.equals("PUT")
				| method.equals("POST")| method.equals("OPTIONS")) {
			return;
		}
		else {
			System.out.println("RequestHandler: check method is: " + method + " not implemented");
			notImplemented = true;
		}
	}
	
	public String requestMethod() {
		return method;
	}

	public String host() {
		return headersMap.get("host");
	}

	public String userAgent() {
		return headersMap.get("user-agent");
	}

	public int port() {
		// TODO Auto-generated method stub
		return WebService.portNum;
	}


	@Override
	public String pathInfo() {
		// TODO Auto-generated method stub
		return this.filePath;
	}


	@Override
	public String url() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String uri() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String protocol() {
		return this.version;
	}


	@Override
	public String contentType() {
		// TODO Auto-generated method stub
		return headersMap.get("content-type");
	}


	@Override
	public String ip() {
		// TODO Auto-generated method stub
		return "121.211.212.11";
	}


	@Override
	public String body() {
		// TODO Auto-generated method stub
		return this.body;
	}


	@Override
	public int contentLength() {
		// TODO Auto-generated method stub
		return Integer.parseInt(headersMap.get("content-length"));
	}


	@Override
	public String headers(String name) {
		return headersMap.get(name);
	}


	@Override
	public Set<String> headers() {
		return headersMap.keySet();
	}


	@Override
	public Session session() {
		return session(true);
	}


	@Override
	public Session session(boolean create) {
		if(sessionId==null) {
			if (create == false) {
				return null;
			}
		}
		currentSession = WebService.getSession(sessionId);
		boolean stillValid = checkSessionValid();
		if(stillValid == false) {
			if(create == false) {
				return null;
			}
			else {   //create a new one
				sessionId = WebService.createSession();
				currentSession =  WebService.getSession(sessionId);
				return currentSession;
			}
		}
		
		else {
			return currentSession;
		}
	
	}
	
	
	
	public Boolean checkSessionValid() {
		if (currentSession == null) {
			return false;
		}
		else {
			long currentTime =  System.currentTimeMillis() / 1000;    // in secs
			boolean stillValid = currentSession.lastAccessedTime() + currentSession.maxInactiveInterval() > currentTime;
			if (stillValid == false) {
				currentSession.invalidate();
				sessionId = null;
				currentSession = null;
				return false;
			}
			else {
				currentSession.access();    //update the last accessed time
				return true;
			}
		}
	}
	
	public String getSessionIdFromHeader() {
		String cookieHeader = headersMap.get("set-cookie");
		if (cookieHeader==null) {
			return null;
		}
		else {
			if (cookieHeader.contains("JSESSIONID")) {
				List<String> splits = new ArrayList<String>(Arrays.asList(cookieHeader.split("[^a-zA-Z0-9']+")));
				int idIndex = splits.indexOf("JSESSIONID");
				return splits.get(idIndex+1);
			}
			else {
				return null;
			}
		}		
	}
	
	public void checkSessionRouting() {
		sessionId = getSessionIdFromHeader();
		if (sessionId!=null) {
		currentSession = WebService.getSession(sessionId);
		checkSessionValid();
		}
	}


	public String getSessionId() {
		return sessionId;
	}
	
	@Override
	public Map<String, String> params() {
		return this.paramMap;
	}
	
	@Override
	public void saveParams(String key, String val) {
		this.paramMap.put(key, val);
	}

	@Override
	public String queryParams(String param) {
		return this.queryParamMap.get(param);
	}
	
	@Override
	public void saveQueryParams(String key, String val) {
		this.queryParamMap.put(key, val);
	}

	@Override
	public List<String> queryParamsValues(String param) {
		// TODO Auto-generated method stub
		return new ArrayList<String>(queryParamMap.values());
	}


	@Override
	public Set<String> queryParams() {
		// TODO Auto-generated method stub
		return queryParamMap.keySet();
	}


	@Override
	public String queryString() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void attribute(String attrib, Object val) {
		this.attributeMap.put(attrib, val);
	}


	@Override
	public Object attribute(String attrib) {
		return this.attributeMap.get(attrib);
	}


	@Override
	public Set<String> attributes() {
		// TODO Auto-generated method stub
		return attributeMap.keySet();
	}


	@Override
	public Map<String, String> cookies() {
		// TODO Auto-generated method stub
		return this.cookieMap;
	}
	@Override
	public boolean isBadRequest() {
		return this.badrequest;
	}
	@Override
	public boolean isNotImplemented() {
		return this.notImplemented;
	}

}

