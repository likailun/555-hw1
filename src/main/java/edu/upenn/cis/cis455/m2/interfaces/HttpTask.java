package edu.upenn.cis.cis455.m2.interfaces;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import edu.upenn.cis.cis455.m1.server.HttpWorker;


import java.io.BufferedReader;
import edu.upenn.cis.cis455.m2.interfaces.RequestHandler1;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.server.WebService;

public class HttpTask {
    Socket requestSocket;
    String rootD;
    ArrayList<HttpWorker> threadsPool;
    Boolean userRouter = false;
    public HttpTask(Socket socket, String rootD, ArrayList<HttpWorker> threadsPool) throws Exception {
    	System.out.println("HttpTask: constructor");
        this.requestSocket = socket;
        this.rootD = rootD;
        this.threadsPool = threadsPool;
        		
    }

    public Socket getSocket() {
        return requestSocket;
    }
    
    public boolean checkSpecial(String path){
    	if (path.equals("/control")| path.equals("/shutdown")) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    public boolean checkRouteWithPath(String pathtest, String routetest, Request requestParser) {
    	String[] pathSplit = pathtest.split("/");
    	String[] routeSplit = routetest.split("/");
    	if (pathSplit.length!=routeSplit.length) {
    		return false;
    	}
    	if (!pathSplit[0].equals(routeSplit[0])) {
    		return false;
    	}
        for (int i = 0; i < pathSplit.length; i++) { 
        	if (routeSplit[i].charAt(0) !=":".charAt(0) && !routeSplit[i].equals("*") && !pathSplit[i].contains("?")) {
        		if (!routeSplit[i].equals(pathSplit[i])) {
        			return false;
        		}
        	}
        	if(routeSplit[i].charAt(0) ==":".charAt(0)) {
        		// set param
        		String param = routeSplit[i].toLowerCase();
        		String val = pathSplit[i];
        		requestParser.saveParams(param, val);
        	}
//        	else if (routeSplit[i].equals("*")) {
//        		
//        	}
        
        	else if (pathSplit[i].contains("?")) {
        		// set queryparam
        		int indexQuestion = pathSplit[i].indexOf("?");
        		String rawQuery = pathSplit[i].substring(indexQuestion+1);
        		String[] params = rawQuery.split("&");  
        	    for (String param : params) {  
        	        String name = param.split("=")[0];  
        	        String value = param.split("=")[1];  
        	        requestParser.saveQueryParams(name, value);  
        	    }  
        	}
        	
        }
        
        
    	return true;
    }
    
    public void run() throws Exception {
    	System.out.print("HttpTask: Running");
    	BufferedReader in = new BufferedReader(new InputStreamReader(requestSocket.getInputStream()));
    	OutputStream outBin = requestSocket.getOutputStream();
        PrintWriter out = new PrintWriter(outBin, true);
        Request requestParser = new RequestHandler1(in);
        requestParser.startParse();
        Response responseHandler = new ResponseHandler(requestParser, out, outBin, rootD, this.threadsPool);
        
        
//        if(requestParser.version.equals("HTTP/1.1")) {
//        	responseHandler.create100response();
//        }
        // init header
        responseHandler.basicHeader();
        
        //check request error and send
        if(requestParser.isNotImplemented() == true | requestParser.isBadRequest() == true) {
        	responseHandler.checkRequestError();
        	responseHandler.sendWithoutBody();
        	return;
        }
                
        // check special and send
        if(checkSpecial(requestParser.pathInfo()) == true) {
        	responseHandler.checkAndSendSpecialRequest();;
        	return;
        }
        
        
        // do before filter 
        
        for (Map.Entry<String, Filter> entry : WebService.beforeMap.entrySet()) {
        	String pathRoute = entry.getKey();
            Filter routeFunction = entry.getValue();
            System.out.println("HttpTask: in before filter");
            // if no user defined route path, just let it handle
            if (pathRoute == "") {
            	routeFunction.handle(requestParser, responseHandler);
            }
            
            else {
            String inputPath = requestParser.pathInfo();
            if (checkRouteWithPath(inputPath, pathRoute, requestParser)==true) {
            	routeFunction.handle(requestParser, responseHandler);
            	System.out.println("HttpTask: in before filer");
            }
      
            }
            
        }
        
        // check user defined router
        
        
        for (Map.Entry<String, Route> entry : WebService.routeMap.entrySet()) {
        	String pathRoute = entry.getKey();
            Route routeFunction = entry.getValue();
            System.out.println("HttpTask: user defined router path is: " + pathRoute);
            String inputPath = requestParser.requestMethod()+requestParser.pathInfo();
            if (checkRouteWithPath(inputPath, pathRoute, requestParser)==true) {
            	userRouter = true;
            	String bodyOutput = (String) routeFunction.handle(requestParser, responseHandler);
            	System.out.println("HttpTask: output body after match is: " + bodyOutput );
            	if (responseHandler.body().equals("")) {
            		responseHandler.body(bodyOutput);
            	}
            	break;
            }
          
        }
        
        // do after filer
        
        
        for (Map.Entry<String, Filter> entry : WebService.beforeMap.entrySet()) {
        	String pathRoute = entry.getKey();
            Filter routeFunction = entry.getValue();
            System.out.println("HttpTask: in after filter");
            // if no user defined route path, just let it handle
            if (pathRoute == "") {
            	routeFunction.handle(requestParser, responseHandler);
            }
            
            else {
            
            String inputPath = requestParser.pathInfo();
            if (checkRouteWithPath(inputPath, pathRoute, requestParser)==true) {
            	routeFunction.handle(requestParser, responseHandler);
            	System.out.println("HttpTask: in after filer");
            }
  
            }
        }
        
        //check session
        String sessionId = requestParser.getSessionId();
        if(sessionId!=null) {
        	responseHandler.cookie("JSESSIONID", sessionId);
        }
        
        if (userRouter == true) {
    	responseHandler.sendWithBody();
    	outBin.close();
        out.close();
        requestSocket.close();
    	return;
        }
        
        
        //if not match use defined router, look for files
       
        
        // get file
        
        responseHandler.findFileAndSend();
        outBin.close();
        out.close();
        requestSocket.close();

        
    }
    
    
    
    
    
    
    
    
}
