package edu.upenn.cis.cis455.m2.interfaces;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.cis455.m2.server.WebService;

public class SessionHandler extends Session{

	public static Map<String, Object> attributeMap = new LinkedHashMap<String, Object>();
	public static long creationTime;
	public static long lastAccessedTime;
	public static String id;
	public static int Timeinterval = 600;   // default interval is 10 minutes
	public SessionHandler(long creationTime) {
		this.creationTime = creationTime;
		this.lastAccessedTime = creationTime;
	}
		
		
	public void setId(String inputId) {
		id = inputId;
	}
	
	@Override
	public String id() {
		return id;
	}

	@Override
	public long creationTime() {
		return creationTime;
	}

	@Override
	public long lastAccessedTime() {
		return lastAccessedTime;
	}

	@Override
	public void invalidate() {
		WebService.deleteSession(id);
	}

	@Override
	public int maxInactiveInterval() {
		return Timeinterval;
	}

	@Override
	public void maxInactiveInterval(int interval) {
		Timeinterval = interval;
	}

	@Override
	public void access() {
		lastAccessedTime = System.currentTimeMillis() / 1000;
		
	}

	@Override
	public void attribute(String name, Object value) {
		attributeMap.put(name, value);
		
	}

	@Override
	public Object attribute(String name) {
		return attributeMap.get(name);
	}

	@Override
	public Set<String> attributes() {
		return attributeMap.keySet();
	}

	@Override
	public void removeAttribute(String name) {
		attributeMap.remove(name);
		
	}

}
