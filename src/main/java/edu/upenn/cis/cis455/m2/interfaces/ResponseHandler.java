package edu.upenn.cis.cis455.m2.interfaces;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.cis455.m1.server.HttpWorker;
import edu.upenn.cis.cis455.m2.server.WebService;
import edu.upenn.cis.cis455.m1.server.HttpListener;

public class ResponseHandler extends Response {
	
	Request request;
	String version;
	String method;
	String path;
	Map<String, String> resHeaders = new HashMap<>();
	String statusCode = "200";
	String statusMessage = "OK";
	String firstLine;
	byte[] body;
	PrintWriter textOutPut;
	OutputStream binOutPut;
	String rootD;
	String pathWithRoot;
	String contentType;
	String contentLength;
	String lastModified;
	ArrayList<HttpWorker> threadsPool;
	boolean finalError = false;
	
	public ResponseHandler(Request requestParser, PrintWriter textOutPut, OutputStream binOutPut, String rootD, ArrayList<HttpWorker> threadsPool) {
		System.out.println("ResponseHandler: constructing");
		this.request = requestParser;
		this.version = requestParser.protocol();
		this.method = requestParser.requestMethod();
		this.path = requestParser.pathInfo();
		this.textOutPut = textOutPut;
		this.binOutPut = binOutPut;
		this.rootD = rootD;
		this.threadsPool = threadsPool;
	}
	
	
	public void basicHeader() {
		// universal init header
		resHeaders.put("Server", "KL/MS1");
		resHeaders.put("Connection", "close");
		resHeaders.put("Date", ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.RFC_1123_DATE_TIME));
	}
	
	public void create100response() {
//		String continue100 = String.format("%s %s %s", "HTTP/1.1","100", "Continue");
		this.statusCode = "100";
		this.statusMessage = "Continue";
		sendWithoutBody();
//		textOutPut.println(continue100);
		
	}
	
	public String createHtml(String status) {
	
	String htmlForm = 	
	"<!DOCTYPE html>\n"+
	"<html>\n"+
	"<head>\n" +
	    "<title>Control Page</title>\n" +
	"</head>\n" +
	"<body>\n" +
	"<h1>Control Page</h1>\n" +
	status +
	"<ul>\n" +
	"<li><a href=\"/shutdown\">Shut down</a></li>\n" +
	"</ul>\n"+
	"</body>\n"+
	"</html>\n";
	
	return htmlForm;
	
	}
	
	public void firstLineResponse() {
		firstLine = String.format( "%s %s %s", this.version, this.statusCode, this.statusMessage);
	}
	
	public void sendWithoutBody() {
		firstLineResponse();
		textOutPut.println(firstLine);
		printHeader();
	}
	
	public void sendWithBody() throws Exception {
		sendWithoutBody();
		if (body.length==0 | this.method.equals("HEAD")) {
			return;
		}
		textOutPut.println();
		binOutPut.write(this.body); // send body
		
	}
	
	public void printHeader() {
	for (Map.Entry<String, String> e : resHeaders.entrySet()) {
        textOutPut.println(String.format("%s: %s", e.getKey(), e.getValue()));
    }
	}
	
	
	public void sendNotFoundError() {
		this.statusCode = "404";
		this.statusMessage = "Not Found";
		sendWithoutBody();
	}
	
	public void setOK() {
		this.statusCode = "200";
		this.statusMessage = "OK";
	}
	
	public void findFileAndSend() throws Exception {
		// check if abs file path
		if (path.contains("http://")) {
			sendNotFoundError();
			return;
		}
		
		pathWithRoot = rootD + path;
		Path pathNotString = Paths.get(pathWithRoot);
		// check if file exist
		if (Files.isRegularFile(pathNotString)) {
			this.body = Files.readAllBytes(pathNotString);
			this.contentLength = String.valueOf(this.body.length);
			this.contentType = Files.probeContentType(pathNotString);
			this.lastModified = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").format(
				    new Date(new File(pathWithRoot).lastModified()) );
			setContentInfo();   // set header
			setOK();      // set first line
			sendWithBody();
		}
		else {
			// file not found
			sendNotFoundError();
			return;
		}
		// check if file modified
	}
	
	// create body
//	public void createBody(String b) {
//		this.body = b.getBytes(Charset.forName("UTF-8"));
//		String contentLength = String.valueOf(this.body.length);
//		resHeaders.put("Content-Length", contentLength);
//		resHeaders.put("Content-Type", "text/plain");
//	}
	
	public void setContentInfo() {
		resHeaders.put("Content-Type", this.contentType);
		resHeaders.put("Content-Length", this.contentLength);
		resHeaders.put("Last-Modified", this.lastModified);
		
	}
	
	public void checkRequestError() {
		if (request.isNotImplemented()==true) {
			statusCode = "501";
			statusMessage = "Not Implemented";
		}
		else if (request.isBadRequest()==true) {
			statusCode = "400";
			statusMessage = "Bad Request";
		}
	}
	
	public void checkAndSendSpecialRequest() throws Exception {
		if (path.equals("/control")) {
			System.out.println("ResponseHandler: control -----");
			setOK();
			String workerStatus = createHtml(getWorkerStatus());
			this.body  = workerStatus.getBytes();
			sendWithBody();
			
			// create body with control panel
			// change to send with body
			
		}
		else if(path.equals("/shutdown")) {
			System.out.println("ResponseHandler: shut down -----");
			// shut down method ---
			HttpListener.stopWorker();
			Thread.sleep(6000);
			WebService.stop();
			setOK();
			sendWithoutBody();
		}
		
	}
	
	
	public String getWorkerStatus(){
//		ArrayList<HttpWorker> a = HttpListener.threadsPool;
		String workerStatus = "";
		for (int i = 0; i < this.threadsPool.size(); i++) {
			HttpWorker worker = threadsPool.get(i);
			workerStatus += "<p> Worker id is: " + worker.id + " Worker status is:" + worker.status +"</p>" + "\n";
		}
		return workerStatus;
	}
	
	
	@Override
	public String getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void header(String header, String value) {
		resHeaders.put(header,value);		
	}


	@Override
	public void redirect(String location) {
		resHeaders.put("Location", location);
		status("301");
//		this.statusMessage = "Moved Permanently";
	}


	@Override
	public void redirect(String location, int httpStatusCode) {
		resHeaders.put("Location", location);
		status(httpStatusCode);		
 	}


	@Override
	public void cookie(String name, String value) {
		String key = "Set-Cookie";
		String val = name + "=" + value;
		header(key,val);
	}


	@Override
	public void cookie(String name, String value, int maxAge) {
		String key = "Set-Cookie";
		String val = name + "=" + value + ";" + "Max-Age=" +maxAge;
		header(key,val);
	}


	@Override
	public void cookie(String name, String value, int maxAge, boolean secured) {
		if (secured==false) {
			cookie(name,value,maxAge);
		}
		else {
		String key = "Set-Cookie";
		String val = name + "=" + value + "; Max-Age=" +maxAge + "; Secure";
		header(key,val);
		}
	}


	@Override
	public void cookie(String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		if(httpOnly==false) {
			cookie(name,value,maxAge,secured);
		}
		else {
			String key = "Set-Cookie";
			String val = name + "=" + value + "; Max-Age=" +maxAge + "; Secure" + "; HttpOnly";
			header(key,val);
		}
	}


	@Override
	public void cookie(String path, String name, String value) {
		String key = "Set-Cookie";
		String val = name + "=" + value + "; Path=" + path;
		header(key,val);		
	}


	@Override
	public void cookie(String path, String name, String value, int maxAge) {
		String key = "Set-Cookie";
		String val = name + "=" + value + "; Path=" + path + "; Max-Age=" +maxAge;
		header(key,val);	
	}


	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured) {
		if (secured==false) {
			cookie(path, name, value, maxAge);
		}
		else {
		String key = "Set-Cookie";
		String val = name + "=" + value + "; Path=" + path + "; Max-Age=" +maxAge + "; Secure";
		header(key,val);
		}		
	}


	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		if(httpOnly==false) {
			cookie(path, name,value,maxAge,secured);
		}
		else {
			String key = "Set-Cookie";
			String val = name + "=" + value + "; Path=" + path + "; Max-Age=" +maxAge + "; Secure" + "; HttpOnly";
			header(key,val);
		}
	}

    public int status() {
        return Integer.parseInt(statusCode);
    }

    public void status(String statusCode) {
        this.statusCode = statusCode;
    }
    
    @Override
    public void status(int statusCode) {
    	this.statusCode = String.valueOf(statusCode);
    }

    public String body() {
        try {
            return body == null ? "" : new String(body, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    public byte[] bodyRaw() {
        return body;
    }

    public void bodyRaw(byte[] b) {
        body = b;
    }

    public void body(String body) {
        this.body = body == null ? null : body.getBytes();
    }

    public String type() {
        return contentType;
    }

    public void type(String contentType) {
        this.contentType = contentType;
        resHeaders.put("Content-Type", this.contentType);
    }
	
	@Override
	public void removeCookie(String name) {
		resHeaders.entrySet().removeIf(e -> e.getValue().contains(name));
	}


	@Override
	public void removeCookie(String path, String name) {
		resHeaders.entrySet().removeIf(e -> e.getValue().contains(name) && e.getValue().contains(path) );
		
	}

}

