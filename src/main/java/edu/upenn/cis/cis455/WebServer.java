package edu.upenn.cis.cis455;

import static edu.upenn.cis.cis455.SparkController.*;

import java.io.IOException;
import java.net.*; 


import org.apache.logging.log4j.Level;

import edu.upenn.cis.cis455.m2.server.WebService;



/**
 * Initialization / skeleton class.
 * Note that this should set up a basic web server for Milestone 1.
 * For Milestone 2 you can use this to set up a basic server.
 * 
 * CAUTION - ASSUME WE WILL REPLACE THIS WHEN WE TEST MILESTONE 2,
 * SO ALL OF YOUR METHODS SHOULD USE THE STANDARD INTERFACES.
 * 
 * @author zives
 *
 */
public class WebServer {
    public static void main(String[] args) {   

        staticFileLocation("./");          
        port(45555);

        get("/testRoute", (request, response) -> {
            return "testRoute content";
        });
            
        get("/:name/hello", (request, response) -> {
            return "Hello: " + request.params("name");
        });

        get("/testCookie1", (request, response) -> {
          String body = "<HTML><BODY><h3>Cookie Test 1</h3>";
          
          response.cookie("TestCookie1", "1");

          body += "Added cookie (TestCookie,1) to response.";
          response.type("text/html");
          response.body(body);
          return response.body();
        });

        get("/testSession1", (request, response) -> {
          String body = "<HTML><BODY><h3>Session Test 1</h3>";

          request.session(true).attribute("Attribute1", "Value1");

          body += "</BODY></HTML>";
          response.type("text/html");
          response.body(body);
          return response.body();
        });

        before((request, response) -> {
          request.attribute("attribute1", "everyone");
        });

        get("/testFilter1", (request, response) -> {
          String body = "<HTML><BODY><h3>Filters Test</h3>";

          for(String attribute : request.attributes()) {
            body += "Attribute: " + attribute + " = " + request.attribute(attribute) + "\n";
          }

          body += "</BODY></HTML>";
          response.type("text/html");
          response.body(body);
          return response.body();
        });

        after((req, res) ->{});
        
        awaitInitialization();
        
        System.out.println("Waiting to handle requests!");
    }

}

//public class WebServer {
//    public static int portNum = 45555;
//    public static String rootDirectory = "./www";
////    public static void main(String[] args) throws Exception {
////        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
////
////        // TODO: make sure you parse *BOTH* command line arguments properly
////        
////        // All user routes should go below here...
////
////        // ... and above here. Leave this comment for the Spark comparator tool
////        
////        if (args.length==1) {
////        	portNum = Integer.valueOf(args[0]);
////        }
////        if (args.length==2) {
////        	portNum = Integer.valueOf(args[0]);
////        	rootDirectory = args[1];
////        }
////        
////        System.out.println("root directory is: " + rootDirectory);
////        WebService webservice = new WebService(portNum, rootDirectory);
////        webservice.start();
////    }
//    
//    public static void main(String[] args) throws Exception {   
//
//        staticFileLocation("./www");          
//        port(45555);
//
//        get("/testRoute", (request, response) -> {
//            return "testRoute content";
//        });
//            
//        get("/:name/hello", (request, response) -> {
//            return "Hello: " + request.params("name");
//        });
////
////        get("/testCookie1", (request, response) -> {
////          String body = "<HTML><BODY><h3>Cookie Test 1</h3>";
////          
////          response.cookie("TestCookie1", "1");
////
////          body += "Added cookie (TestCookie,1) to response.";
////          response.type("text/html");
////          response.body(body);
////          return response.body();
////        });
////
////        get("/testSession1", (request, response) -> {
////          String body = "<HTML><BODY><h3>Session Test 1</h3>";
////
////          request.session(true).attribute("Attribute1", "Value1");
////
////          body += "</BODY></HTML>";
////          response.type("text/html");
////          response.body(body);
////          return response.body();
////        });
////
//        before((request, response) -> {
//          request.attribute("attribute1", "everyone");
//        });
//
//        get("/testFilter1", (request, response) -> {
//          String body = "<HTML><BODY><h3>Filters Test</h3>";
//
//          for(String attribute : request.attributes()) {
//            body += "Attribute: " + attribute + " = " + request.attribute(attribute) + "\n";
//          }
//
//          body += "</BODY></HTML>";
//          response.type("text/html");
//          response.body(body);
//          return response.body();
//        });
//
//        after((req, res) ->{});
//        
//        awaitInitialization();
//        
//        System.out.println("Waiting to handle requests!");
//    }
//    
//}
