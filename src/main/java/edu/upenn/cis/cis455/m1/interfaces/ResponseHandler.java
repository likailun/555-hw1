package edu.upenn.cis.cis455.m1.interfaces;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.cis455.m1.server.HttpWorker;
import edu.upenn.cis.cis455.m1.server.WebService;
import edu.upenn.cis.cis455.m1.server.HttpListener;

public class ResponseHandler {
	
	RequestHandler request;
	String version;
	String method;
	String path;
	Map<String, String> resHeaders = new HashMap<>();
	String statusCode = "200";
	String statusMessage = "OK";
	String firstLine;
	byte[] body;
	PrintWriter textOutPut;
	OutputStream binOutPut;
	String rootD;
	String pathWithRoot;
	String contentType;
	String contentLength;
	String lastModified;
	ArrayList<HttpWorker> threadsPool;
	boolean finalError = false;
	
	public ResponseHandler(RequestHandler request, PrintWriter textOutPut, OutputStream binOutPut, String rootD, ArrayList<HttpWorker> threadsPool) {
		System.out.println("ResponseHandler: constructing");
		this.request = request;
		this.version = request.version;
		this.method = request.method;
		this.path = request.filePath;
		this.textOutPut = textOutPut;
		this.binOutPut = binOutPut;
		this.rootD = rootD;
		this.threadsPool = threadsPool;
	}
	
	
	public void basicHeader() {
		// universal init header
		resHeaders.put("Server", "KL/MS1");
		resHeaders.put("Connection", "close");
		resHeaders.put("Date", ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.RFC_1123_DATE_TIME));
	}
	
	public void create100response() {
//		String continue100 = String.format("%s %s %s", "HTTP/1.1","100", "Continue");
		this.statusCode = "100";
		this.statusMessage = "Continue";
		sendWithoutBody();
//		textOutPut.println(continue100);
		
	}
	
	public String createHtml(String status) {
	
	String htmlForm = 	
	"<!DOCTYPE html>\n"+
	"<html>\n"+
	"<head>\n" +
	    "<title>Control Page</title>\n" +
	"</head>\n" +
	"<body>\n" +
	"<h1>Control Page</h1>\n" +
	status +
	"<ul>\n" +
	"<li><a href=\"/shutdown\">Shut down</a></li>\n" +
	"</ul>\n"+
	"</body>\n"+
	"</html>\n";
	
	return htmlForm;
	
	}
	
	public void firstLineResponse() {
		firstLine = String.format( "%s %s %s", this.version, this.statusCode, this.statusMessage);
	}
	
	public void sendWithoutBody() {
		firstLineResponse();
		textOutPut.println(firstLine);
		printHeader();
	}
	
	public void sendWithBody() throws Exception {
		sendWithoutBody();
		if (this.body==null | this.method.equals("HEAD")) {
			return;
		}
		textOutPut.println();
		binOutPut.write(this.body); // send body
		
	}
	
	public void printHeader() {
	for (Map.Entry<String, String> e : resHeaders.entrySet()) {
        textOutPut.println(String.format("%s: %s", e.getKey(), e.getValue()));
    }
	}
	
	
	public void sendNotFoundError() {
		this.statusCode = "404";
		this.statusMessage = "Not Found";
		sendWithoutBody();
	}
	
	public void setOK() {
		this.statusCode = "200";
		this.statusMessage = "OK";
	}
	
	public void findFileAndSend() throws Exception {
		// check if abs file path
		if (path.contains("http://")) {
			sendNotFoundError();
			return;
		}
		
		pathWithRoot = rootD + path;
		Path pathNotString = Paths.get(pathWithRoot);
		// check if file exist
		if (Files.isRegularFile(pathNotString)) {
			this.body = Files.readAllBytes(pathNotString);
			this.contentLength = String.valueOf(this.body.length);
			this.contentType = Files.probeContentType(pathNotString);
			this.lastModified = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").format(
				    new Date(new File(pathWithRoot).lastModified()) );
			setContentInfo();   // set header
			setOK();      // set first line
			sendWithBody();
		}
		else {
			// file not found
			sendNotFoundError();
			return;
		}
		// check if file modified
	}
	
	// create body
//	public void createBody(String b) {
//		this.body = b.getBytes(Charset.forName("UTF-8"));
//		String contentLength = String.valueOf(this.body.length);
//		resHeaders.put("Content-Length", contentLength);
//		resHeaders.put("Content-Type", "text/plain");
//	}
	
	public void setContentInfo() {
		resHeaders.put("Content-Type", this.contentType);
		resHeaders.put("Content-Length", this.contentLength);
		resHeaders.put("Last-Modified", this.lastModified);
		
	}
	
	public void checkRequestError() {
		if (request.notImplemented==true) {
			statusCode = "501";
			statusMessage = "Not Implemented";
		}
		else if (request.badrequest==true) {
			statusCode = "400";
			statusMessage = "Bad Request";
		}
	}
	
	public void checkAndSendSpecialRequest() throws Exception {
		if (path.equals("/control")) {
			System.out.println("ResponseHandler: control -----");
			setOK();
			String workerStatus = createHtml(getWorkerStatus());
			this.body  = workerStatus.getBytes();
			sendWithBody();
			
			// create body with control panel
			// change to send with body
			
		}
		else if(path.equals("/shutdown")) {
			System.out.println("ResponseHandler: shut down -----");
			// shut down method ---
			HttpListener.stopWorker();
			Thread.sleep(6000);
			WebService.stop();
			setOK();
			sendWithoutBody();
		}
		
	}
	
	
	public String getWorkerStatus(){
//		ArrayList<HttpWorker> a = HttpListener.threadsPool;
		String workerStatus = "";
		for (int i = 0; i < this.threadsPool.size(); i++) {
			HttpWorker worker = threadsPool.get(i);
			workerStatus += "<p> Worker id is: " + worker.id + " Worker status is:" + worker.status +"</p>" + "\n";
		}
		return workerStatus;
	}
	
	

}
