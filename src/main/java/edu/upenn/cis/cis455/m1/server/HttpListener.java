package edu.upenn.cis.cis455.m1.server;

import java.util.ArrayList;
import java.util.HashMap;
import edu.upenn.cis.cis455.m2.interfaces.HttpTask;
/**
 * Stub for your HTTP server, which listens on a ServerSocket and handles
 * requests
 */
public class HttpListener implements Runnable {

	public int numberThreads;
	public int sizeQueue;
	public HttpTaskQueue sharedQueue;
	public static ArrayList<HttpWorker> threadsPool; 
	public HttpListener(int numberThreads, int sizeQueue) {
		System.out.println("inside Http Listener constructor");
		this.sizeQueue = sizeQueue;
		this.sharedQueue = new HttpTaskQueue(sizeQueue);
		this.numberThreads = numberThreads;
		threadsPool =  new ArrayList<>();
	}
	
    @Override
    public void run() {
		for (int i = 0; i < this.numberThreads; i++)  {
    		System.out.println("HttpListener: creating worker");
    		HttpWorker worker = new HttpWorker(sharedQueue, i);
    		worker.start();
    		threadsPool.add(worker);
    	}
    	System.out.println("HttpListener: running");
    }
    
    public synchronized void addNewTask(HttpTask task) {
    	System.out.println("add new task in httplistener");
		sharedQueue.add(task);
	}
    
    public static synchronized void stopWorker() {
    	for(HttpWorker worker : threadsPool){
    		System.out.print("HttpListener: stop each worker");
            worker.stopEachWorker(); 
            if(worker.stopped==true) {
            	worker.interrupt();
            	System.out.print("HttpListener: one worker interrupt");
            }
         }
    }
    
    
}
