package edu.upenn.cis.cis455.m1.interfaces;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class RequestHandler extends Request {

	public String version = "HTTP/1.0";
	public String filePath;
	public String method;
	public HashMap<String, String> headersMap = new HashMap<>();
	public BufferedReader inputRequest;
	public Boolean badrequest = false;
	public Boolean notImplemented = false;
	
	public RequestHandler(BufferedReader inputRequest) {
		this.inputRequest = inputRequest;
	}
	
	
	public void startParse() throws Exception {
		System.out.println("RequestHandler: start parsing");
		getFirst();
		getHeader();
		System.out.println("RequestHandler: file path is: " + filePath 
				+ "method is:" + method +"\n"
				+ "header is " + Arrays.asList(headersMap)
				);	
		checkFirst();
	}
	
	public void getFirst() throws Exception {
		String firstInput = this.inputRequest.readLine();
		String[] firstTemp;
		if (firstInput != null) {
			firstTemp = firstInput.split("\\s+");
			
			if (firstTemp.length < 3) {
				badrequest = true;
			}
			else {
			method = firstTemp[0];
			filePath = firstTemp[1];
			version = firstTemp[2];
			}
		}
		if(firstInput == null) {
			badrequest = true;
		}
		
	}
	
	public void getHeader() throws Exception{
		String line = inputRequest.readLine();
		String lastKey = null;
        while (line != null && !line.trim().isEmpty()) {
            int p = line.indexOf(':');
            if (p >= 0) {
            	String key = line.substring(0, p).trim().toLowerCase(Locale.US);
            	String value = line.substring(p + 1).trim();
            	lastKey = key;
            	headersMap.put(key, value);
            } else if (lastKey != null && line.startsWith(" ") || line.startsWith("\t")) {
                String newPart = line.trim();
                headersMap.put(lastKey, headersMap.get(lastKey) + newPart);
            }
            line = inputRequest.readLine();
        }
	}
	
	public void checkFirst() {
		checkHost();
		checkMethod();
	}
	
	public void checkHost() {
		if(version.equals("HTTP/1.1") && host()==null) {
			badrequest = true;
		}
	}
	
	// check if get or head
	public void checkMethod() {
		if (method.equals("GET") | method.equals("HEAD")) {
			return;
		}
		else {
			System.out.println("RequestHandler: check method is: " + method + " not implemented");
			notImplemented = true;
		}
	}
	
	@Override
	public String requestMethod() {
		// TODO Auto-generated method stub
		return method;
	}

	@Override
	public String host() {
		// TODO Auto-generated method stub
		return headersMap.get("host");
	}

	@Override
	public String userAgent() {
		// TODO Auto-generated method stub
		return headersMap.get("user-agent");
	}

	@Override
	public int port() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String pathInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String url() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String uri() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String protocol() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String contentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String ip() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String body() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int contentLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String headers(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> headers() {
		// TODO Auto-generated method stub
		return null;
	}

}
