/**
 * CIS 455/555 route-based HTTP framework
 * 
 * V. Liu, Z. Ives
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m1.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;

import java.io.IOException;
import java.net.*; 

public class WebService {
	
	public String rootD;
	public int portNum;
	// change number of threads and max size of shared queue
	int numThreads = 10;
	int sizeSharedQ = 10000;
	public static ServerSocket serverSocket;
	public WebService(int port, String rootDirect) {
		System.out.println("inside webservice port and rootDirect are:" + rootDirect);
		this.portNum = port; 
		this.rootD = rootDirect;
	}
	
    final static Logger logger = LogManager.getLogger(WebService.class);

    protected HttpListener listener;

    /**
     * Launches the Web server thread pool and the listener
     * @throws Exception 
     */
    public void start() throws Exception {
        serverSocket = new ServerSocket(portNum);
        listener = new HttpListener(numThreads, sizeSharedQ);
        listener.run();
		while (true) {
			Socket clientSocket = serverSocket.accept();
			System.out.println("WebService.java starts");
			System.out.println(" Webservice.java Accepted connection: " + clientSocket);
//			listener.addNewTask(new HttpTask(clientSocket, rootD, listener.threadsPool));
		}
    }

    /**
     * Gracefully shut down the server
     * @throws Exception 
     */
    public static void stop() throws Exception {
    	System.out.println("WebService: stop the server");
    	serverSocket.close();
    	System.out.println("WebService: all system closed");
    	return;
    }

    /**
     * Hold until the server is fully initialized.
     * Should be called after everything else.
     * @throws Exception 
     */
    public void awaitInitialization() throws Exception {
        logger.info("Initializing server");
        start();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt() {
        throw new HaltException();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode) {
        throw new HaltException(statusCode);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(String body) {
        throw new HaltException(body);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode, String body) {
        throw new HaltException(statusCode, body);
    }

    ////////////////////////////////////////////
    // Server configuration
    ////////////////////////////////////////////

    /**
     * Set the root directory of the "static web" files
     */
    public void staticFileLocation(String directory) {
    }

    /**
     * Set the IP address to listen on (default 0.0.0.0)
     */
    public void ipAddress(String ipAddress) {}

    /**
     * Set the TCP port to listen on (default 45555)
     */
    public void port(int port) {}

    /**
     * Set the size of the thread pool
     */
    public void threadPool(int threads) {}

}
