package edu.upenn.cis.cis455.m1.server;

import java.util.LinkedList;
import edu.upenn.cis.cis455.m2.interfaces.HttpTask;
/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
	LinkedList<HttpTask> sharedQueue; 
	int sizeQ;
	
	public HttpTaskQueue(int sizeQ) {
		this.sizeQ = sizeQ;
		sharedQueue = new LinkedList<>(); 
		System.out.println("inside HttpTaskQueue constructor");
	}
	
	public synchronized void add(HttpTask task) {
		// if queue is full then wait
		while (sharedQueue.size() == this.sizeQ) {
			try {
				System.out.println("shared queue currently full");
				wait();	 
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 		
		}
		if (sharedQueue.size() == 0) {
			notify(); 
		}
		sharedQueue.add(task);
		System.out.println("new task added inside HttpTaskQueue, current queue size is " + sharedQueue.size());
	}
	
	public synchronized HttpTask remove() {
		while (sharedQueue.isEmpty()) {
			try {
				System.out.println("HttpTaskQueue: queue empty, worker waiting");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}

		if (sharedQueue.size() == this.sizeQ) { 
			notify(); 
		} 
		
		HttpTask item = sharedQueue.remove(0);
		return item;
	}
	
	
	public synchronized int size() {
		return sharedQueue.size(); 
	}

}
