package edu.upenn.cis.cis455.m1.server;
import edu.upenn.cis.cis455.m2.interfaces.HttpTask;

/**
 * Stub class for a thread worker that handles Web requests
 */
public class HttpWorker extends Thread implements Runnable {

	// while true, take task from the queue and populate request class
    
    public HttpTaskQueue sharedQ;
    public int id;
    public String status;
    public boolean stopping = false;
    public boolean stopped = false;
    public HttpWorker(HttpTaskQueue sharedQ, int id) {
    	this.sharedQ  = sharedQ;
    	this.id = id;
    	this.status = "waiting";
    }
    
    public void stopEachWorker() {
    	this.stopping = true;
    }
    
    @Override
    public void run() {
        System.out.println("HttpWorker: This is inside worker with id: " + this.id);
        while(this.stopping == false) {
        	this.status = "waiting";
        	HttpTask task = sharedQ.remove();
        	this.status = "working";
        	try {
				task.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("HttpWorker: failed to run task");
			}
        	System.out.println("Worker: " + this.id +  " finished 1 task");
        	
        }
        System.out.print("worker " + this.id + "stopped");
        this.stopped = true;
    }
}
