package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import java.io.BufferedReader;

import edu.upenn.cis.cis455.m1.interfaces.RequestHandler;
import edu.upenn.cis.cis455.m1.interfaces.ResponseHandler;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.server.WebService;

public class HttpTask {
    Socket requestSocket;
    String rootD;
    ArrayList<HttpWorker> threadsPool;
    public HttpTask(Socket socket, String rootD, ArrayList<HttpWorker> threadsPool) throws Exception {
    	System.out.println("HttpTask: constructor");
        this.requestSocket = socket;
        this.rootD = rootD;
        this.threadsPool = threadsPool;
        		
    }

    public Socket getSocket() {
        return requestSocket;
    }
    
    public boolean checkSpecial(String path){
    	if (path.equals("/control")| path.equals("/shutdown")) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    public void run() throws Exception {
    	System.out.print("HttpTask: Running");
    	BufferedReader in = new BufferedReader(new InputStreamReader(requestSocket.getInputStream()));
    	OutputStream outBin = requestSocket.getOutputStream();
        PrintWriter out = new PrintWriter(outBin, true);
        
        RequestHandler requestParser = new RequestHandler(in);
        requestParser.startParse();
        ResponseHandler responseHandler = new ResponseHandler(requestParser, out, outBin, rootD, this.threadsPool);
        
        
//        if(requestParser.version.equals("HTTP/1.1")) {
//        	responseHandler.create100response();
//        }
        // init header
        responseHandler.basicHeader();
        
        //check request error and send
        if(requestParser.notImplemented == true | requestParser.badrequest == true) {
        	responseHandler.checkRequestError();
        	responseHandler.sendWithoutBody();
        	requestSocket.close();
        	return;
        }
                
        // check special and send
        if(checkSpecial(requestParser.filePath) == true) {
        	responseHandler.checkAndSendSpecialRequest();;
        	requestSocket.close();
        	return;
        }
        
        
        Iterator it = WebService.routeMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            String pathRoute = (String) pair.getKey();
            Route routeFunction = (Route) pair.getValue();
            System.out.println("HttpTask: path is: " + pathRoute);
            it.remove();
            if (pathRoute.equals(requestParser.method+requestParser.filePath)) {
            	String bodyOutput = (String) routeFunction.handle(null, null);
            	System.out.println("HttpTask: output body after match is: " + bodyOutput );
            }
            
        }        
        
       
        
        // get file
        
        responseHandler.findFileAndSend();

        outBin.close();
        out.close();
        requestSocket.close();
        
    }
    
    
    
    
    
    
    
    
}
